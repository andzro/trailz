package com.andzro.myapplication;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MapList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_list);

        CountDownTimer countdown = new CountDownTimer(3000,1000) {
            @Override
            public void onTick(long l) {
                Log.i("Tick", String.valueOf(l));
            }

            @Override
            public void onFinish() {
                Intent intent = new Intent(getApplicationContext(),MenuActivity.class);
                startActivity(intent);


            }
        };

        countdown.start();

    }
}
