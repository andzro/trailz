package com.andzro.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Map;


public class MainActivity extends AppCompatActivity {
    ImageView imageViewMap;
    ImageView imageViewMapHide1;
    ImageView imageViewMapHide2;
    ImageView imageViewMapHide3;
    ImageView imageViewMapHide4;
    ImageView imageViewMapHide5;
    ImageView imageViewMapHide6;
    Button buttonSave;
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewMap = (ImageView)findViewById(R.id.imageViewMap);
        imageViewMapHide1 = (ImageView)findViewById(R.id.imageViewHide1);
        imageViewMapHide2 = (ImageView)findViewById(R.id.imageViewHide2);
        imageViewMapHide3 = (ImageView)findViewById(R.id.imageViewHide3);
        imageViewMapHide4 = (ImageView)findViewById(R.id.imageViewHide4);
        imageViewMapHide5 = (ImageView)findViewById(R.id.imageViewHide5);
        imageViewMapHide6 = (ImageView)findViewById(R.id.imageViewHide6);

        buttonSave = (Button)findViewById(R.id.buttonSave);
        imageViewMap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                counter++;
                if(counter==1)
                {
                    imageViewMapHide1.setVisibility(View.VISIBLE);
                }
                if(counter==2)
                {
                    imageViewMapHide2.setVisibility(View.VISIBLE);
                }
                if(counter==3)
                {
                    imageViewMapHide3.setVisibility(View.VISIBLE);
                }
                if(counter==4)
                {
                    imageViewMapHide4.setVisibility(View.VISIBLE);
                }
                if(counter==5)
                {
                    imageViewMapHide5.setVisibility(View.VISIBLE);
                }
                if(counter==6)
                {
                    imageViewMapHide6.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });


        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),MenuActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(),"Successfuly saved map data!",Toast.LENGTH_SHORT).show();
            }
        });




    }


}
